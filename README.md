
maomao
==================================================
[![PyPI version](https://badge.fury.io/py/maomao.svg)](https://badge.fury.io/py/maomao)
[![License](https://img.shields.io/badge/License-BSD%203--Clause-blue.svg)](https://opensource.org/licenses/BSD-3-Clause)

Maomao translator.

Installation
-------------------------

```bash
pip3 install maomao
```

Examples
-------------------------

### English to Maomao

```bash
maomao This is just a test sentence.
```

output:

```output
MaOo mAOo mAO mAoo mao mAO mAoo mao mAAAO maoO mAoo maOo mao maO mao maOo mAo mAoo maOo mao mAoo mAo maOO maOo mAo maOO maaO mAo .
```

### Maomao to English

```bash
maomao MaOo mAOo mAO mAoo mao mAO mAoo mao mAAAO maoO mAoo maOo mao maO mao maOo mAo mAoo maOo mao mAoo mAo maOO maOo mAo maOO maaO mAo .
```

output:

```output
This is just a test sentence.
```


